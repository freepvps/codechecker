from .token import TokenType
from .tokenparser import tokenize
from .custom_parser import parse as parse_code


def extract_features(s):
    """
    :type s: basestring
    :param s: input code
    :return: features list
    :rtype: list[int]
    """
    return parse_code(tokenize(s))
